package smart.smartsocket.model;

public class HistoryList {
    String tanggal, jam, kwh, ket;

    public HistoryList(String tanggal, String jam, String kwh, String ket) {
        this.tanggal = tanggal;
        this.jam = jam;
        this.kwh = kwh;
        this.ket = ket;
    }

    public HistoryList(String tanggal, String jam, String kwh) {
        this.tanggal = tanggal;
        this.jam = jam;
        this.kwh = kwh;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getKwh() {
        return kwh;
    }

    public void setKwh(String kwh) {
        this.kwh = kwh;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }
}
