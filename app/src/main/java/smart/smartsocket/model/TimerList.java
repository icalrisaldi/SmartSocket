package smart.smartsocket.model;

import java.util.ArrayList;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TimerList extends RealmObject {
    @PrimaryKey
    int timeId;

    Integer startTimeJam, startTimeMenit, endTimeJam, endTimeMenit;
    String jenisSocket;
    boolean status;

    public TimerList() {
    }

    public TimerList(int timeId, Integer startTimeJam, Integer startTimeMenit, Integer endTimeJam, Integer endTimeMenit, String jenisSocket, boolean status) {
        this.timeId = timeId;
        this.startTimeJam = startTimeJam;
        this.startTimeMenit = startTimeMenit;
        this.endTimeJam = endTimeJam;
        this.endTimeMenit = endTimeMenit;
        this.jenisSocket = jenisSocket;
        this.status = status;
    }

    public int getTimeId() {
        return timeId;
    }

    public void setTimeId(int timeId) {
        this.timeId = timeId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getJenisSocket() {
        return jenisSocket;
    }

    public void setJenisSocket(String jenisSocket) {
        this.jenisSocket = jenisSocket;
    }

    public Integer getStartTimeJam() {
        return startTimeJam;
    }

    public void setStartTimeJam(Integer startTimeJam) {
        this.startTimeJam = startTimeJam;
    }

    public Integer getStartTimeMenit() {
        return startTimeMenit;
    }

    public void setStartTimeMenit(Integer startTimeMenit) {
        this.startTimeMenit = startTimeMenit;
    }

    public Integer getEndTimeJam() {
        return endTimeJam;
    }

    public void setEndTimeJam(Integer endTimeJam) {
        this.endTimeJam = endTimeJam;
    }

    public Integer getEndTimeMenit() {
        return endTimeMenit;
    }

    public void setEndTimeMenit(Integer endTimeMenit) {
        this.endTimeMenit = endTimeMenit;
    }

}
