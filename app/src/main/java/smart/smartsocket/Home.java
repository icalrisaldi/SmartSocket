package smart.smartsocket;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import smart.smartsocket.History.History;
import smart.smartsocket.Setting.Alarm;

public class Home extends AppCompatActivity {
    ImageView monitor, history, setting, pairing;
    Switch socket1, socket2;
    Context context;
    ProgressDialog progressDialog;
    String status_socket1 = "0", status_socket2 = "0";
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Inisialisasi Firebase
        context = this;
        databaseReference = FirebaseDatabase.getInstance().getReference("smartSocket/statusSocket");

        //Inisialisai Realm Database untuk database lokal
        Realm.init(this);
        RealmConfiguration realmConfiguration =
                new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);

        //Inisialisasi UI
        monitor = findViewById(R.id.btn_monitor);
        history = findViewById(R.id.btn_history);
        setting = findViewById(R.id.btn_setting);
        pairing = findViewById(R.id.btn_pairing);
        pairing.setOnClickListener(handleClick);
        monitor.setOnClickListener(handleClick);
        history.setOnClickListener(handleClick);
        setting.setOnClickListener(handleClick);

        //Inisialisasi Progress Dialog
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        //Inisialisasi switch
        socket1 = findViewById(R.id.socket1);
        socket1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    databaseReference.child("socket1").setValue("1");
                    status_socket1 = "1";
                } else {
                    databaseReference.child("socket1").setValue("0");
                    status_socket1 = "0";
                }
            }
        });
        socket2 = findViewById(R.id.socket2);
        socket2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    databaseReference.child("socket2").setValue("1");
                    status_socket2 = "1";
                } else {
                    databaseReference.child("socket2").setValue("0");
                    status_socket2 = "0";
                }
            }
        });

        setStatus();

    }

    private View.OnClickListener handleClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.btn_monitor:
                    intent = new Intent(Home.this, Monitor.class);
                    intent.putExtra("status_socket1", status_socket1);
                    intent.putExtra("status_socket2", status_socket2);
                    startActivity(intent);
                    break;
                case R.id.btn_history:
                    intent = new Intent(Home.this, History.class);
                    startActivity(intent);
                    break;
                case R.id.btn_setting:
                    intent = new Intent(Home.this, Alarm.class);
                    startActivity(intent);
                    break;
                case R.id.btn_pairing:
                    intent = new Intent(Home.this, Pairing.class);
                    startActivity(intent);
                    break;
            }
        }
    };


    public void setStatus() {
        //Mengambil data status on/off socket dari firebase
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (dataSnapshot.child("socket1").getValue().toString().equals("0")) {
                        socket1.setChecked(false);
                    } else if (dataSnapshot.child("socket1").getValue().toString().equals("1")) {
                        socket1.setChecked(true);
                    }

                    if (dataSnapshot.child("socket2").getValue().toString().equals("0")) {
                        socket2.setChecked(false);
                    } else if (dataSnapshot.child("socket2").getValue().toString().equals("1")) {
                        socket2.setChecked(true);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
