package smart.smartsocket;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Monitor extends AppCompatActivity {
    TextView txt_arus1, txt_tegangan1, txt_daya1, txt_arus2, txt_tegangan2, txt_daya2;
    String status_socket1 = "0", status_socket2 = "0";
    Context context;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swipeRefreshLayout;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);

        context = this;

        //Insialisasi Firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("smartSocket");

        //Inisialisasi UI
        txt_arus1 = findViewById(R.id.txt_arus_1);
        txt_tegangan1 = findViewById(R.id.txt_tegangan_1);
        txt_daya1 = findViewById(R.id.txt_daya_1);
        txt_arus2 = findViewById(R.id.txt_arus_2);
        txt_tegangan2 = findViewById(R.id.txt_tegangan_2);
        txt_daya2 = findViewById(R.id.txt_daya_2);

        //Mendapatkan Data status on/off socket dari home
        Intent intent = getIntent();
        status_socket1 = intent.getStringExtra("status_socket1");
        status_socket2 = intent.getStringExtra("status_socket2");

        //Inisialisasi fungsi progres dialog dan swipe to refresh
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Jika refresh maka akan mengambil data
                swipeRefreshLayout.setRefreshing(true);
                getDataSocket();
            }
        });

        //Method untuk mengambil data
        getDataSocket();

        //Inisialisasi Toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Monitor");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getDataSocket() {
        //Mengambil data Dengan Firebase
        databaseReference.child("data").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (status_socket1.equals("1")) {
                        txt_arus1.setText(dataSnapshot.child("socket1").child("arus").getValue().toString());
                        txt_tegangan1.setText(dataSnapshot.child("socket1").child("tegangan").getValue().toString());
                        txt_daya1.setText(dataSnapshot.child("socket1").child("daya").getValue().toString());
                    }

                    if (status_socket2.equals("1")) {
                        txt_arus2.setText(dataSnapshot.child("socket2").child("arus").getValue().toString());
                        txt_tegangan2.setText(dataSnapshot.child("socket2").child("tegangan").getValue().toString());
                        txt_daya2.setText(dataSnapshot.child("socket2").child("daya").getValue().toString());
                    }
                } else {
                    Toast.makeText(context, "Swipe kebawah untuk refresh", Toast.LENGTH_SHORT).show();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
