package smart.smartsocket.db;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.realm.Realm;
import io.realm.exceptions.RealmException;
import smart.smartsocket.model.TimerList;

public class DatabaseCrud {
    //    final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("smartSocket/timerSocket");
    String statusSocket;

    public void setTimer(Integer[] startTime, Integer[] endTime, boolean status, String jenisSocket) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Number currentIdNum = realm.where(TimerList.class).max("timeId");
        int nextId;
        if (currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }

        TimerList timerList = realm.createObject(TimerList.class, nextId);
        timerList.setStartTimeJam(startTime[0]);
        timerList.setStartTimeMenit(startTime[1]);
        timerList.setEndTimeJam(endTime[0]);
        timerList.setEndTimeMenit(endTime[1]);
        timerList.setStatus(status);
        timerList.setJenisSocket(jenisSocket);
        realm.commitTransaction();
        realm.close();

        if (status) {
            statusSocket = "1";
        } else {
            statusSocket = "0";
        }
//        databaseReference.child(String.valueOf(nextId)).child("startTime").setValue(startTime);
//        databaseReference.child(String.valueOf(nextId)).child("endTime").setValue(endTime);
//        databaseReference.child(String.valueOf(nextId)).child("jenisSocket").setValue(statusSocket);
//        databaseReference.child(String.valueOf(nextId)).child("status").setValue(jenisSocket);

    }

    public void updateTimer(int timeId, Integer[] startTime, Integer[] endTime, boolean status, String jenisSocket) throws RealmException {

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        TimerList timerList = realm.where(TimerList.class).equalTo("timeId", timeId).findFirst();
        timerList.setStartTimeJam(startTime[0]);
        timerList.setStartTimeMenit(startTime[1]);
        timerList.setEndTimeJam(endTime[0]);
        timerList.setEndTimeMenit(endTime[1]);
        timerList.setStatus(status);
        timerList.setJenisSocket(jenisSocket);
        realm.copyToRealmOrUpdate(timerList);
        realm.commitTransaction();
        realm.close();

        if (status) {
            statusSocket = "1";
        } else {
            statusSocket = "0";
        }
//        databaseReference.child(String.valueOf(timeId)).child("startTime").setValue(startTime);
//        databaseReference.child(String.valueOf(timeId)).child("endTime").setValue(endTime);
//        databaseReference.child(String.valueOf(timeId)).child("status").setValue(statusSocket);
    }

    public void updateTimerStatus(int timeId, boolean status) throws RealmException {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        TimerList timerList = realm.where(TimerList.class).equalTo("timeId", timeId).findFirst();
        timerList.setStatus(status);
        realm.copyToRealmOrUpdate(timerList);
        realm.commitTransaction();
        realm.close();

        if (status) {
            statusSocket = "1";
        } else {
            statusSocket = "0";
        }
//        databaseReference.child(String.valueOf(timeId)).child("status").setValue(statusSocket);
    }

    public void deleteTimer(int timeId) throws RealmException {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        TimerList timerList = realm.where(TimerList.class).equalTo("timeId", timeId).findFirst();
        timerList.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
//        databaseReference.child(String.valueOf(timeId)).removeValue();
    }
}
