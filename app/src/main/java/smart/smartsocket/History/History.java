package smart.smartsocket.History;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import smart.smartsocket.R;
import smart.smartsocket.model.HistoryList;

public class History extends AppCompatActivity {
    RecyclerView lv_history;
    Context context;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swipeRefreshLayout;
    DatabaseReference databaseReference;
    DatabaseReference historyReset;
    FloatingActionButton btn_reset;

    HistoryRecycleAdapter mAdapter;
    ArrayList<HistoryList> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        context = this;
        databaseReference = FirebaseDatabase.getInstance().getReference("history");
        historyReset = FirebaseDatabase.getInstance().getReference("smartSocket").child("log").child("logHistory").child("id");
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Mengambil Data");

        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getHistory();
            }
        });

        btn_reset = findViewById(R.id.fab);
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.removeValue();
                historyReset.setValue(0);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("History");

        lv_history = findViewById(R.id.lv_history);
        lv_history.setHasFixedSize(false);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        lv_history.setLayoutManager(mLayoutManager);

        // Inside the onCreate before setting the Adapter to RecyclerView
        // Here we are setting a divider padding of 20
        lv_history.addItemDecoration(new ItemOffsetDecoration(20));

        // Initialize and set the RecyclerView Adapter
        mList = new ArrayList<>();
        mAdapter = new HistoryRecycleAdapter(mList);/*
        mList.add(new HistoryList("Tanggal","Jam","Kwh"));*/
        lv_history.setAdapter(mAdapter);

        progressDialog.show();
        getHistory();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getHistory(){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    mList.clear();
                    /*//Uncomment salah satu, Jika tampilan data terbaru dibawah, data lama diatas
                    mList.add(new HistoryList("","",""));*/

                    for(DataSnapshot str : dataSnapshot.getChildren()){
                        if(!isNull(str.child("tanggal").getValue()) && !isNull(str.child("jam").getValue()) && !isNull(str.child("kwh").getValue())) {
                            mList.add(new HistoryList(str.child("tanggal").getValue().toString(), str.child("jam").getValue().toString(), str.child("kwh").getValue().toString()));
                        }
                    }

                    //Uncomment salah satu, Jika tampilan data terbaru diatas, data lama dibawah
                    mList.add(new HistoryList("","",""));
                    Collections.reverse(mList);
                    mAdapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    Toast.makeText(context, "History Kosong", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(context, "Swipe Untuk Refresh", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
        private int offset;

        public ItemOffsetDecoration(int offset) {
            this.offset = offset;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {

            // Add padding only to the zeroth item
            if (parent.getChildAdapterPosition(view) == 0) {

                outRect.right = offset;
                outRect.left = offset;
                outRect.top = offset;
                outRect.bottom = offset;
            }
        }
    }

    public boolean isNull(Object obj){
        if(obj==null){
            return true;
        } else {
            return false;
        }
    }

}
