package smart.smartsocket.History;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import smart.smartsocket.R;
import smart.smartsocket.model.HistoryList;

public class HistoryListAdapter extends BaseAdapter {

    private Context mContext;
    private List<HistoryList> HistoryLists;

    //Constructor

    public HistoryListAdapter(Context mContext, List<HistoryList> HistoryLists) {
        this.mContext = mContext;
        this.HistoryLists = HistoryLists;
    }


    @Override
    public int getCount() {
        return HistoryLists.size();
    }

    @Override
    public Object getItem(int position) {
        return HistoryLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mContext, R.layout.item_list_history, null);

        TextView txtTanggal = (TextView) v.findViewById(R.id.txt_tanggal);
        TextView txtJam = (TextView) v.findViewById(R.id.txt_jam);
        TextView txtKwh = (TextView) v.findViewById(R.id.txt_kwh);/*
        TextView txtKet = (TextView) v.findViewById(R.id.txt_ket);*/
        //Set text for TextView
        txtTanggal.setText(HistoryLists.get(position).getTanggal());
        txtJam.setText(HistoryLists.get(position).getJam());
        txtKwh.setText(HistoryLists.get(position).getKwh());
        /*txtKet.setText(HistoryLists.get(position).getKet());*/
        return v;
    }
}
