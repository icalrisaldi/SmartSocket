package smart.smartsocket.History;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import smart.smartsocket.R;
import smart.smartsocket.model.HistoryList;

public class HistoryRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    ArrayList<HistoryList> mList = new ArrayList<>();

    public HistoryRecycleAdapter(ArrayList<HistoryList> mList) {

        this.mList = mList;
        this.notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;

        }

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_history, viewGroup, false);
            return new ItemViewHolder(view);

        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_list_history, viewGroup, false);
            return new HeaderViewHolder(view);

        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof HeaderViewHolder) {
            //set the Value from List to corresponding UI component as shown below.

            //similarly bind other UI components or perform operations

        } else if (holder instanceof ItemViewHolder) {
            ((ItemViewHolder) holder).txtTanggal.setText(mList.get(position).getTanggal());
            ((ItemViewHolder) holder).txtJam.setText(mList.get(position).getJam());
            ((ItemViewHolder) holder).txtKwh.setText(mList.get(position).getKwh());


            // Your code here

        }


    }


    @Override
    public int getItemCount() {
        // Add two more counts to accomodate header and footer
        if(mList.size()==0){
            return 0;
        } else {
            return this.mList.size();
        }
    }


    // The ViewHolders for Header, Item and Footer
    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public View View;
        public TextView txtTanggal, txtJam, txtKwh;

//        private final TextView txtName;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            View = itemView;
            txtTanggal = View.findViewById(R.id.txt_tanggal);
            txtJam = View.findViewById(R.id.txt_jam);
            txtKwh = View.findViewById(R.id.txt_kwh);

//            // add your ui components here like this below

        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public View View;
        public TextView txtTanggal, txtJam, txtKwh;

        public ItemViewHolder(View v) {
            super(v);
            View = v;
            txtTanggal = View.findViewById(R.id.txt_tanggal);
            txtJam = View.findViewById(R.id.txt_jam);
            txtKwh = View.findViewById(R.id.txt_kwh);
            // Add your UI Components here

        }

    }
}
