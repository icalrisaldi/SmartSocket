package smart.smartsocket.Setting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MyAlarm extends BroadcastReceiver {
    DatabaseReference databaseReference;

    //the method will be fired when the alarm is triggerred
    @Override
    public void onReceive(Context context, Intent intent) {
        String jenis = intent.getStringExtra("jenis");
        String status = intent.getStringExtra("status");

        databaseReference = FirebaseDatabase.getInstance().getReference("smartSocket/statusSocket");
        if (status.equals("start")) {
            databaseReference.child(jenis).setValue(1);
        } else if (status.equals("end")) {
            databaseReference.child(jenis).setValue(0);
        }

        //you can check the log that it is fired
        //Here we are actually not doing anything
        //but you can do any task here that you want to be done at a specific time everyday
        Log.d("MyAlarmBelal", "Alarm just fired");
        Toast.makeText(context, "Alarm Fired", Toast.LENGTH_SHORT).show();

    }
}
