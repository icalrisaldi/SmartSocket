package smart.smartsocket.Setting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ikovac.timepickerwithseconds.MyTimePickerDialog;

import java.lang.reflect.Array;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import io.realm.Realm;
import io.realm.RealmResults;
import smart.smartsocket.R;
import smart.smartsocket.db.DatabaseCrud;
import smart.smartsocket.model.TimerList;

public class Alarm extends AppCompatActivity {

    TextView txt_timer1, txt_timer2;
    ImageView btn_setting;
    Switch swtc_status1, swtc_status2;
    Context context;
    DatabaseReference databaseReference;
    ProgressDialog progressDialog;
    Integer[] startTime, endTime;
    DatabaseCrud databaseCrud;
    Realm realm;
    RealmResults<TimerList> realmResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        context = this;
        databaseReference = FirebaseDatabase.getInstance().getReference("smartSocket/statusSocket");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Setting Timer");

        databaseCrud = new DatabaseCrud();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sinkronisasi");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        txt_timer1 = findViewById(R.id.txt_time);
        txt_timer2 = findViewById(R.id.txt_time2);
        btn_setting = findViewById(R.id.btn_setting);
        btn_setting.setOnClickListener(handleClick);
        swtc_status1 = findViewById(R.id.swtc_status);
        swtc_status1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                databaseCrud.updateTimerStatus(1, b);
                if (b) {
                    getTimer();
                } else {
                    cancelAlarm(0);
                    cancelAlarm(1);
                    databaseReference.child("socket1").setValue("0");
                }
            }
        });
        swtc_status2 = findViewById(R.id.swtc_status2);
        swtc_status2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                databaseCrud.updateTimerStatus(2, b);
                if (b) {
                    getTimer();
                } else {
                    cancelAlarm(2);
                    cancelAlarm(3);
                    databaseReference.child("socket2").setValue("0");
                }
            }
        });

        getTimer();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return true;
    }


    private View.OnClickListener handleClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_setting:
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                    LayoutInflater inflater = getLayoutInflater();

                    View mView = inflater.inflate(R.layout.pop_up_setting, null);

                    mBuilder.setView(mView);
                    final AlertDialog dialogAlert = mBuilder.create();
                    dialogAlert.show();

                    Button btn_pilih = mView.findViewById(R.id.btn_pilih);
                    final EditText edt_start_time = mView.findViewById(R.id.edt_start_time);
                    final EditText edt_end_time = mView.findViewById(R.id.edt_end_time);
                    final CheckBox chk_socket1 = mView.findViewById(R.id.chk_socket1);
                    final CheckBox chk_socket2 = mView.findViewById(R.id.chk_socket2);

                    startTime = new Integer[2];
                    endTime = new Integer[2];


                    edt_start_time.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startTime = timePicker(edt_start_time);
                        }
                    });
                    edt_end_time.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            endTime = timePicker(edt_end_time);
                        }
                    });

                    btn_pilih.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            boolean chk_status_socket1 = false, chk_status_socket2 = false;
                            if (edt_end_time.getText().toString().equals("") || edt_start_time.getText().toString().equals("")) {
                                Toast.makeText(context, "Waktu tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            } else {
                                if (chk_socket1.isChecked()) {
                                    chk_status_socket1 = true;
                                }

                                if (chk_socket2.isChecked()) {
                                    chk_status_socket2 = true;
                                }

                                if (chk_status_socket1 || chk_status_socket2) {
                                    setTimeStatus(startTime, endTime, edt_start_time.getText().toString(), edt_end_time.getText().toString(), chk_status_socket1, chk_status_socket2);
                                    dialogAlert.dismiss();
                                } else {
                                    Toast.makeText(context, "Pilih socket yang akan di set", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    });
                    break;
            }
        }
    };

    public void setTimeStatus(Integer[] startTimeparse, Integer[] endTimeparse, String startTime, String endTime, boolean chk_status_socket1, boolean chk_status_socket2) {
        if (chk_status_socket1 && chk_status_socket2) {
            databaseCrud.setTimer(startTimeparse, endTimeparse, true, "socket1");
            databaseCrud.setTimer(startTimeparse, endTimeparse, true, "socket2");
            setAlarm(startTimeparse, 0, "socket1", "start");
            setAlarm(endTimeparse, 1, "socket1", "end");
            setAlarm(startTimeparse, 2, "socket2", "start");
            setAlarm(endTimeparse, 3, "socket2", "end");
        } else if (chk_status_socket2) {
            databaseCrud.setTimer(startTimeparse, endTimeparse, true, "socket2");
            setAlarm(startTimeparse, 2, "socket2", "start");
            setAlarm(endTimeparse, 3, "socket2", "end");
        } else if (chk_status_socket1) {
            databaseCrud.setTimer(startTimeparse, endTimeparse, true, "socket1");
            setAlarm(startTimeparse, 0, "socket1", "start");
            setAlarm(endTimeparse, 1, "socket1", "end");
        }
        getTimer();
    }

    public void getTimer() {
        realm = Realm.getDefaultInstance();
        realmResults = realm.where(TimerList.class).findAll();

        Integer[] startTime = new Integer[2];
        Integer[] endTime = new Integer[2];

        if (realmResults.size() != 0) {
            for (TimerList str : realmResults) {
                switch (str.getJenisSocket()) {
                    case "socket1":
                        txt_timer1.setText(String.valueOf(str.getStartTimeJam()) + ":" + String.valueOf(str.getStartTimeMenit()) + " - "
                                + String.valueOf(str.getEndTimeJam()) + ":" + String.valueOf(str.getEndTimeMenit()));
                        swtc_status1.setChecked(str.isStatus());
                        if (str.isStatus()) {
                            startTime[0] = str.getStartTimeJam();
                            startTime[1] = str.getStartTimeMenit();
                            endTime[0] = str.getEndTimeJam();
                            endTime[1] = str.getEndTimeMenit();
                            setAlarm(startTime, 0, "socket1", "start");
                            setAlarm(endTime, 1, "socket1", "end");
                        }
                        break;
                    case "socket2":
                        txt_timer2.setText(String.valueOf(str.getStartTimeJam()) + ":" + String.valueOf(str.getStartTimeMenit()) + " - "
                                + String.valueOf(str.getEndTimeJam()) + ":" + String.valueOf(str.getEndTimeMenit()));
                        swtc_status2.setChecked(str.isStatus());
                        if (str.isStatus()) {
                            startTime[0] = str.getStartTimeJam();
                            startTime[1] = str.getStartTimeMenit();
                            endTime[0] = str.getEndTimeJam();
                            endTime[1] = str.getEndTimeMenit();
                            setAlarm(startTime, 2, "socket2", "start");
                            setAlarm(endTime, 3, "socket2", "end");
                        }
                        break;
                }
            }
        }

    }

    public void setAlarm(Integer[] myDateString, int ID, String jenis, String status) {
        //SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        //the above commented line was changed to the one below, as per Grodriguez's pertinent comment:

        Date dat = new Date();
        Calendar cal_alarm = Calendar.getInstance();
        Calendar cal_now = Calendar.getInstance();
        cal_now.setTime(dat);
        cal_alarm.setTime(dat);
        cal_alarm.set(Calendar.HOUR_OF_DAY, myDateString[0]);
        cal_alarm.set(Calendar.MINUTE, myDateString[1] - 1);
        cal_alarm.set(Calendar.SECOND, 30);
        if (cal_alarm.before(cal_now)) {
            cal_alarm.add(Calendar.DATE, 1);
        }

        //getting the alarm manager
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        //creating a new intent specifying the broadcast receiver
        Intent i = new Intent(this, MyAlarm.class);
        i.putExtra("jenis", jenis);
        i.putExtra("status", status);

        //creating a pending intent using the intent
        PendingIntent pi = PendingIntent.getBroadcast(this, ID, i, 0);

        //setting the repeating alarm that will be fired every day
        am.setRepeating(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);

    }

    public void cancelAlarm(int ID) {
        //getting the alarm manager
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //creating a new intent specifying the broadcast receiver
        Intent i = new Intent(this, MyAlarm.class);
        //creating a pending intent using the intent
        PendingIntent pi = PendingIntent.getBroadcast(this, ID, i, 0);
        am.cancel(pi);
    }

    public Integer[] timePicker(final EditText editText) {
        final Integer[] date = new Integer[2];
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editText.setText(selectedHour + ":" + selectedMinute);
                date[0] = selectedHour;
                date[1] = selectedMinute;
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();


        return date;
    }
}
