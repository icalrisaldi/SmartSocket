package smart.smartsocket;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Pairing extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pairing);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pairing");

        WebView webView = findViewById(R.id.wv_pairing);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://google.com"); //Untuk webview diganti sesuai dengan keinginan / Alamat
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
